# Build-in

- werkzeug

# Run Flask app
export FLASK_APP=app.py
export FLASK_ENV=development
set FLASK_APP=app.py (for windows)

flask run

# Static files
http://127.0.0.1:5000/static/android.png

# Templates
from flask import Flask, render_template

# Errors
from flask import abort
abort(404)

# Forms
    from flask import request
    @app.route('/add_card', methods=['GET', 'POST'])
    if request.method == "POST":
        ...

    <form></form>: will add params from form at url part
    <form method="post"></form> will not, just send "post"

# Validation Forms (flask-wtf)
https://flask-wtf.readthedocs.io/en/stable/

# Redirect
from flask import url_for, redirect

# Style
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="/static/style.css">
    <title>Flash Card</title>
</head>

# Deploy
- gunicorn
- reverse proxy (nginx)

## On a Server
- ssh on server
- install ubuntu(apt)/alpine(apk), gunicorn, nginx, python3, python3-pip, git
(~docker, docker-compose)
- install dependencies
- sudo apt install gunicorn3
- gunicorn3 -D app:app (demon/run at background)
- cd /etc/nginx/site-available/
- sudo rm default
- sudo nano default (https://gunicorn.org/#deployment; file > nginx.conf)
  - more nginx conf: https://docs.gunicorn.org/en/latest/deploy.html#nginx-configuration
  - flask options: https://flask.palletsprojects.com/en/1.1.x/deploying/
- sudo service nginx restart