from flask import (
    Flask,
    render_template,
    abort,
    jsonify,
    request,
    url_for,
    redirect
)

# flask constructor
from model import db, save_db

app = Flask(__name__)


# Return TEXT
# @app.route('/')
# def hello_world():
#     return 'Welcome to Flask Base App!'

@app.route('/')
def welcome():
    return render_template(
        "welcome.html",
        message="Some message here!"
    )


counter = 0


@app.route('/views')
def count_views():
    global counter
    counter += 1
    return f'Views count: {counter}'


@app.route('/card/<int:index>')
def card_view(index):  # /card/1
    try:
        card = db[index]
        return render_template(
            "card.html",
            card=card,
            index=index,
            max_index=len(db)-1,
        )
    except IndexError:
        abort(404)


@app.route('/add_card', methods=['GET', 'POST'])
def add_card():
    if request.method == "POST":
        # form has been submitted. process data
        card = {
            "question": request.form["question"],
            "answer": request.form["answer"],
        }
        db.append(card)
        save_db()
        # return redirect("http://www.example.com")
        # show last card (that was added)
        return redirect(url_for("card_view", index=len(db)-1))
    else:
        # get
        return render_template("add_card.html")


# BACKEND: REST API
@app.route("/api/card/")
def api_card_list():
    return jsonify(db)  # list -> json


@app.route("/api/card/<int:index>")
def api_card_detail(index):
    try:
        return db[index]
    except IndexError:
        abort(404)


if __name__ == '__main__':
    app.run()
